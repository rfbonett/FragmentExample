package cs.wm.edu.fragmenttest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;

public class LeakyFragment extends Fragment {

    public LeakyFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Basic leak from Calendar.getTimeZone() to Log.d()
        String data = Calendar.getInstance().getTimeZone().getDisplayName();
        Log.d("leak", data);

        return inflater.inflate(R.layout.fragment_leaky, container, false);
    }
}
